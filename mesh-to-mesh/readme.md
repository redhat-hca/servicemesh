proxy [main] ⚡  helm upgrade -i mongodb bitnami/mongodb -n mongodb

WARNING: Kubernetes configuration file is group-readable. This is insecure. Location: /var/home/eboyer/.kube/config
Release "mongodb" does not exist. Installing it now.
I0621 16:19:46.298112  220313 request.go:655] Throttling request took 1.196512331s, request: GET:https://api.ipi.hcaocp.com:6443/apis/packages.operators.coreos.com/v1?timeout=32s
NAME: mongodb
LAST DEPLOYED: Mon Jun 21 16:19:47 2021
NAMESPACE: mongodb
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
** Please be patient while the chart is being deployed **

MongoDB(R) can be accessed on the following DNS name(s) and ports from within your cluster:

    mongodb.mongodb.svc.cluster.local

To get the root password run:

    export MONGODB_ROOT_PASSWORD=$(kubectl get secret --namespace mongodb mongodb -o jsonpath="{.data.mongodb-root-password}" | base64 --decode)

To connect to your database, create a MongoDB(R) client container:

    kubectl run --namespace mongodb mongodb-client --rm --tty -i --restart='Never' --env="MONGODB_ROOT_PASSWORD=$MONGODB_ROOT_PASSWORD" --image docker.io/bitnami/mongodb:4.4.6-debian-10-r8 --command -- bash

Then, run the following command:
    mongo admin --host "mongodb" --authenticationDatabase admin -u root -p $MONGODB_ROOT_PASSWORD

To connect to your database from outside the cluster execute the following commands:

    kubectl port-forward --namespace mongodb svc/mongodb 27017:27017 &
    mongo --host 127.0.0.1 --authenticationDatabase admin -p $MONGODB_ROOT_PASSWORD
