#!/bin/bash

. ./00_env.sh
oc -n ${APP_NAMESPACE} delete route {details,productpage,ratings,reviews} --ignore-not-found=true
