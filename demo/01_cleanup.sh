#!/bin/bash

. ./00_env.sh

function cleanup() {
  # clear SM_NAMESPACE things
  oc -n ${SM_NAMESPACE} delete route {details,productpage,ratings,reviews}-gateway --ignore-not-found=true
  oc -n ${SM_NAMESPACE} delete gateway bookinfo-wildcard-gateway --ignore-not-found=true
  oc -n ${SM_NAMESPACE} delete secret istio-ingressgateway-certs --ignore-not-found=true
  oc -n ${SM_NAMESPACE} delete smmr default --ignore-not-found=true

  # clear APP_NAMESPACE
  oc -n ${APP_NAMESPACE} delete destinationrule {details,productpage,ratings,reviews}-client-mtls --ignore-not-found=true
  oc -n ${APP_NAMESPACE} delete virtualservice {details,productpage,ratings,reviews}-virtualservice --ignore-not-found=true
  oc -n ${APP_NAMESPACE} delete peerauthentication {details,productpage,ratings,reviews}-mtls --ignore-not-found=true

  # clear bookinfo deployment
  oc delete -f ${BOOKINFO_YAML_URL} --ignore-not-found=true
  #oc delete deployment --all -n bookinfo
  echo "Wait for OpenShift to clean up project=${APP_NAMESPACE}..."
  sleep 15
}

echo "Cleaning up $SM_NAMESPACE and $APP_NAMESPACE ..."

cleanup