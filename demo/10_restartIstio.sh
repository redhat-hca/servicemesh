#!/bin/bash

# restart istio ingress pod
oc -n ${SM_NAMESPACE} patch deployment istio-ingressgateway -p '{"spec":{"template":{"metadata":{"annotations":{"kubectl.kubernetes.io/restartedAt": "'$(date +%FT%T%z)'"}}}}}' 
