#!/bin/bash

. ./00_env.sh

function installServiceMeshMemberRoll() {
  echo "Adding ${1} to ServiceMesh"
  oc apply -n ${SM_NAMESPACE} -f ./config/smmr-bookinfo.yaml
}

function injectSidecars() {
  echo "Injecting envoy proxies"
  for name in $(listResource deployment); do
    oc patch deployment/$name -p '{"spec":{"template":{"metadata":{"annotations":{"sidecar.istio.io/inject": "true"}}}}}' -n ${APP_NAMESPACE}
  done
}

projectToAdd=${APP_NAMESPACE}

if [ ! -z "${1}" ] 
then
  projectToAdd=${1}
fi

installServiceMeshMemberRoll $projectToAdd
injectSidecars