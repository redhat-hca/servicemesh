#!/bin/bash

. ./00_env.sh

echo "Setup complete!"
GATEWAY_URL=$(oc -n ${SM_NAMESPACE} get route productpage-gateway -o jsonpath='{.spec.host}')
echo "App can be accessed at https://${GATEWAY_URL}"
echo "Example: curl -o /dev/null -s -w \"%{http_code}\\n\" https://$GATEWAY_URL/productpage --cacert tls.crt"