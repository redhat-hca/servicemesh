#!/bin/bash

. ./00_env.sh

DOMAIN=${DOMAIN_NAME}.${TOP_LEVEL_DOMAIN}

openssl req -x509 -sha256 -nodes -days 365 -newkey rsa:2048 \
    -subj '/O=${DOMAIN_NAME} Inc./CN=${DOMAIN_NAME}.${TOP_LEVEL_DOMAIN}' \
    -keyout ${DOMAIN_NAME}.${TOP_LEVEL_DOMAIN}.key \
    -out ${DOMAIN_NAME}.${TOP_LEVEL_DOMAIN}.crt

openssl req -out ${APP_NAMESPACE}.${DOMAIN}.csr -newkey rsa:2048 -nodes \
    -keyout ${APP_NAMESPACE}.${DOMAIN}.key \
    -subj "/CN=${APP_NAMESPACE}.${DOMAIN}/O=${APP_NAMESPACE} organization"

openssl x509 -req -days 365 -CA ${DOMAIN}.crt -CAkey ${DOMAIN}.key -set_serial 0 \
    -in ${APP_NAMESPACE}.${DOMAIN}.csr \
    -out ${APP_NAMESPACE}.${DOMAIN}.crt

oc create -n ${SM_NAMESPACE} secret tls ${APP_NAMESPACE}-${DOMAIN_NAME} \
    --key=${APP_NAMESPACE}.${DOMAIN}.key \
    --cert=${APP_NAMESPACE}.${DOMAIN}.crt