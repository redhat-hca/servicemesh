#!/bin/bash

APP_NAMESPACE=bookinfo
SM_NAMESPACE=istio-system
DOMAIN_NAME=digitaldissent
TOP_LEVEL_DOMAIN=io
BOOKINFO_YAML_URL=https://raw.githubusercontent.com/Maistra/istio/maistra-2.1/samples/bookinfo/platform/kube/bookinfo.yaml

function listResource() {
  oc get $1 -n ${APP_NAMESPACE} | tail -n +2 | awk '{print $1}'
}

function redeployApps() {
  for name in $(listResource deployment); do
    restartAppViaDeployment $name
  done
}

function restartAppViaDeployment() {
  [[ -n $1 ]] && {
    oc patch deployment/$1 -p '{"spec":{"template":{"metadata":{"annotations":{"kubectl.kubernetes.io/restartedAt": "'$(date +%s)'"}}}}}' -n ${APP_NAMESPACE}
  }
}