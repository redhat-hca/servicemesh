#!/bin/bash

. ./00_env.sh

function createmTLSResources() {
  echo ">>> Creating Gateway ..."
  oc apply -n ${SM_NAMESPACE} -f ./manifests/gateway-https.yaml 

  # Create DestinationRules
  echo ">>> Creating DestinationRule(s) ..."
  

  # Create VirtualServices
  echo ">>> Creating VirtualService(s) ..."
  oc create -n ${APP_NAMESPACE} -f ./manifests/virtualservice.yaml
}

echo "6.1 mTLS Security - No readiness or liveness checks exists, skipping addition of command-based checks (note: already included in Service Mesh setup)"
echo "6.2 mTLS Security - Creating mTLS Policy for ${APP_NAMESPACE}"
echo "6.3 mTLS Security - Create resources necessary for mTLS"
createmTLSResources

echo "6.3 mTLS Security - Creating SSL cert and private key for gateway"
createSelfSignedCertAndPrivateKey

echo "6.3 mTLS Security - Clean up old route(s)"

echo "Restarting istio-ingressgateway and rolling out latest app deployments"
oc patch deployment istio-ingressgateway -p '{"spec":{"template":{"metadata":{"annotations":{"kubectl.kubernetes.io/restartedAt": "'$(date +%FT%T%z)'"}}}}}' -n ${SM_NAMESPACE}
redeployApps
