# servicemesh

## Overview

This guide is intended to showcase Service Mesh integration on the latest version of OpenShift 4.7. The application used to demonstrate these capabilities will be [bookinfo](https://istio.io/latest/docs/examples/bookinfo/). 

## Prerequisites

- OpenShift Elasticserch Operator
- Red Hat OpenShift Jaeger Operator
- Kiali Operator
- Red Hat OpenShift Service Mesh
- OpenShift CLI tool

- Set the following environment variables:
    ```
    APP_NAMESPACE=bookinfo
    SM_NAMESPACE=istio-system
    DOMAIN_NAME=<your_domain>
    TOP_LEVEL_DOMAIN=<your_tld> 
    BOOKINFO_YAML_URL=https://raw.githubusercontent.com/Maistra/istio/maistra-2.1/samples/bookinfo/platform/kube/bookinfo.yaml
    ```

Note `DOMAIN_NAME` and `TOP_LEVEL_DOMAIN` are variables that are used to generate self-signed certificates. If you already have certificates, use those instead.

1) Create project and install the default control plane:

    ```
    oc new-project ${SM_NAMESPACE}
    oc apply -n ${SM_NAMESPACE} -f config/smcp.yaml 
    ```

2) Wait for the control plan pods to reach `Running` state.

    ```
    oc get pods -n ${SM_NAMESPACE} -w
    ```

    You should see 10 pods running:

    ```
    grafana-7f5b75bf55-wnmvc                 2/2     Running   0          31m
    istio-citadel-8588c6d84c-jqd2t           1/1     Running   0          32m
    istio-galley-7fb4d57b6c-ptj99            1/1     Running   0          31m
    istio-pilot-77d4bdc658-8frr6             2/2     Running   0          31m
    istio-policy-5bf44c55ff-k7s6g            2/2     Running   0          31m
    istio-sidecar-injector-66d8fd5fd-7cvl2   1/1     Running   0          31m
    istio-telemetry-6d65c74644-7wswz         2/2     Running   0          31m
    jaeger-6cd56dcb8-mpmbm                   2/2     Running   0          32m
    kiali-644d77bcc-vrzbz                    1/1     Running   0          30m
    prometheus-65dbc4d76b-r92zh              2/2     Running   0          32m
    ```

3) Deploy application (bookinfo in this example)

    ```
    oc new-project ${APP_NAMESPACE}
    oc apply -n ${APP_NAMESPACE} -f ${BOOKINFO_YAML_URL}
    ```

4) Test application. Get route and open within browser:

    ```
    oc expose svc/productpage -n ${APP_NAMESPACE}
    oc get route productpage -n ${APP_NAMESPACE}
    ```

5) Add application project as a service mesh member. Wait for pods to reach 2/2 `Running`.

    ```
    oc apply -n ${SM_NAMESPACE} -f ./config/smmr-bookinfo.yaml
    oc get pods -n ${APP_NAMESPACE} -w
    ```

6) Generate certificate and private key, and create secret

    ```
    ./bin/generateCertsAndSecret.sh
    ```

7) Generate gateway (front door into service mesh); 

    ```
    oc apply -n ${APP_NAMESPACE} -f ./config/gateway-https.yaml 
    ```

    | Take note of the route that's automatically generated in the SMCP

8) Add CNAME record to point bookinfo subdomain to the OpenShift load balancers. This is typically a manual edit through the domain's registrar. `nslookup` can be used to identify the address for the load balancers. 

9) Generate virtual service

    ```
    oc apply -n ${APP_NAMESPACE} -f ./config/virtualservice.yaml
    ```

10) Test application. Get route and open in browser

    ```
    echo "https://$(oc get route bookinfo -n ${SM_NAMESPACE} -o jsonpath={'.spec.host'})/productpage"
    ```